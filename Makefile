BIN_FILE := bin/copper-cli
PKG := gitlab.com/capinside/copper-cli
PKG_LIST := $(shell go list $(PKG)/... | grep -v /vendor/)

.PHONY: all clean dep test coverage

all: get build install test coverage

build: dep ## Build binary
	go build -ldflags="-s -w" -o $(BIN_FILE) main.go

clean:
	rm $(BIN_FILE)
	
dep:
	@go get -v -d $(PKG_LIST)

get: ## Get dependencies
	go get $(PKG_LIST)

install:
	go install $(PKG_LIST)

test: ## Run unittests
	go test $(PKG_LIST) -v -coverprofile coverage.txt
	go tool cover -func coverage.txt

race: ## Run data race detector
	go test -race -v $(PKG_LIST)

msan: ## Run memory sanitizer
	go test -msan -v $(PKG_LIST)

coverage: test ## Generate global code coverage report
	go tool cover -html=coverage.txt

help: ## Display this help screen
  @grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'