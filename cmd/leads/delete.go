package leads

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
)

var (
	unused    bool
	deleteCmd = &cobra.Command{
		Use:   "delete",
		Short: "Delete lead(s). Arguments must be the ID(s) of lead(s) to delete.",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 && !unused {
				return fmt.Errorf("requires either an argument or --unused")
			}
			return nil
		},
		RunE: deleteRunEFunc,
	}
)

func init() {
	deleteCmd.Flags().Int64VarP(&maximumCreatedDate, "maximum-created-date", "", -1, "The Unix timestamp of the latest date a resource was created.")
	deleteCmd.Flags().Int64VarP(&maximumInteractionDate, "maximum-interaction-date", "", -1, "The Unix timestamp of the latest date of the last interaction.")
	deleteCmd.Flags().Int64VarP(&minimumCreatedDate, "minimum-created-date", "", -1, "The Unix timestamp of the earliest date the resource was created.")
	deleteCmd.Flags().Int64VarP(&minimumInteractionDate, "minimum-interaction-date", "", -1, "The Unix timestamp of the earliest date of the last interaction.")
	deleteCmd.Flags().Int64VarP(&maximumInteractionCount, "maximum-interaction-count", "", -1, "The maximum number of interactions the resource must have had.")
	deleteCmd.Flags().BoolVarP(&unused, "unused", "", false, "Delete all unused leads")
}

func deleteRunEFunc(cmd *cobra.Command, args []string) error {
	var leadIDs []int64
	var err error

	if unused {
		leadIDs, err = unusedLeadIDs()
	} else {
		leadIDs, err = utils.StringSliceToInt64Slice(args)
	}
	if err != nil {
		return err
	}

	return delete(leadIDs)
}

func unusedLeadIDs() ([]int64, error) {
	params := unusedSearchParams()
	leads, err := api.SearchAllLeads(params)
	if err != nil {
		return nil, err
	}

	result := make([]int64, len(leads))
	for i, lead := range leads {
		result[i] = *lead.Id
	}

	return result, nil
}

func delete(IDs []int64) error {
	if viper.GetBool("dry-run") {
		return nil
	}

	for _, id := range IDs {
		_, err := api.DeleteLead(id)
		if err != nil {
			return err
		}
	}

	return nil
}
