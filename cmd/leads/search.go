package leads

import (
	"time"

	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	searchParams = copperv1.LeadSearchParams{}

	searchCmd = &cobra.Command{
		Use:   "search",
		Short: "Search leads",
		RunE:  searchRunEFunc,
	}
)

func init() {
	addSearchParams(searchCmd)
}

func searchRunEFunc(cmd *cobra.Command, args []string) error {
	if minimumInteractionCount > -1 {
		searchParams.MinimumInteractionCount = &minimumInteractionCount
	}

	if maximumInteractionCount > -1 {
		searchParams.MaximumInteractionCount = &maximumInteractionCount
	}

	if minimumInteractionDate > -1 {
		searchParams.MinimumInteractionDate = &minimumInteractionDate
	}

	if maximumInteractionDate > -1 {
		searchParams.MaximumInteractionDate = &maximumInteractionDate
	}

	if minimumCreatedDate > -1 {
		searchParams.MinimumCreatedDate = &minimumCreatedDate
	}

	if maximumCreatedDate > -1 {
		searchParams.MaximumCreatedDate = &maximumCreatedDate
	}

	result, err := api.SearchAllLeads(searchParams)
	if err != nil {
		return err
	}

	return output(result)
}

func unusedSearchParams() copperv1.LeadSearchParams {
	maximumCreatedDate := time.Date(2018, 12, 4, 23, 59, 59, 0, time.UTC).Unix()
	maximumInteractionCount := utils.Int64Addr(0)
	minimumInteractionCount := utils.Int64Addr(0)
	minimumCreatedDate := time.Date(2018, 12, 4, 0, 0, 0, 0, time.UTC).Unix()

	return copperv1.LeadSearchParams{
		MaximumCreatedDate:      &maximumCreatedDate,
		MaximumInteractionCount: maximumInteractionCount,
		MinimumCreatedDate:      &minimumCreatedDate,
		MinimumInteractionCount: minimumInteractionCount,
		SortBy:                  "name",
	}
}

func addSearchParams(cmd *cobra.Command) {
	cmd.Flags().Int64VarP(&maximumCreatedDate, "maximum-created-date", "", -1, "The Unix timestamp of the latest date a resource was created.")
	cmd.Flags().Int64VarP(&maximumInteractionDate, "maximum-interaction-date", "", -1, "The Unix timestamp of the latest date of the last interaction.")
	cmd.Flags().Int64VarP(&minimumCreatedDate, "minimum-created-date", "", -1, "The Unix timestamp of the earliest date the resource was created.")
	cmd.Flags().Int64VarP(&minimumInteractionDate, "minimum-interaction-date", "", -1, "The Unix timestamp of the earliest date of the last interaction.")
	cmd.Flags().Int64VarP(&maximumInteractionCount, "maximum-interaction-count", "", -1, "The maximum number of interactions the resource must have had.")
	cmd.Flags().Int64VarP(&minimumInteractionCount, "minimum-interaction-count", "", -1, "The minimum number of interactions the resource must have had.")
	cmd.PersistentFlags().StringVarP(&searchParams.City, "city", "", "", "City")
	cmd.PersistentFlags().StringVarP(&searchParams.Emails, "emails", "", "", "Email addresses")
	cmd.PersistentFlags().StringVarP(&searchParams.Name, "name", "", "", "Full name")
}
