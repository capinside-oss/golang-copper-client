package companies

import (
	"encoding/json"
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	companiesCmd = &cobra.Command{
		Use:   "companies",
		Short: "Company commands",
	}
)

func init() {
	companiesCmd.AddCommand(
		customFieldCmd,
		deleteCmd,
		fetchByIDCmd,
		searchCmd,
	)
}

// Command returns a pointer to the Cobra Command
func Command() *cobra.Command {
	return companiesCmd
}

func output(companies []*copperv1.Company) error {
	switch viper.GetString("output") {
	case "wide":
		return outputWide(companies)
	default:
		return outputJSON(companies)
	}
}

func outputWide(companies []*copperv1.Company) error {
	w := tabwriter.NewWriter(os.Stdout, 10, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "%s\t%s\t%s\t%s\n", "ID", "Name", "City", "Email Domain")

	for _, company := range companies {
		fmt.Fprintf(w, "%d\t%s\t%s\t%s\n", company.Id, company.Name, company.Address.City, company.EmailDomain)
	}

	return nil
}

func outputJSON(companies []*copperv1.Company) error {
	return json.NewEncoder(os.Stdout).Encode(companies)
}

func outputDeleteResponse(delResponses []*copperv1.DeleteResponse) error {
	switch viper.GetString("output") {
	case "wide":
		return outputDeleteResponseWide(delResponses)
	default:
		return outputDeleteResponseJSON(delResponses)
	}
}

func outputDeleteResponseJSON(delResponses []*copperv1.DeleteResponse) error {
	return json.NewEncoder(os.Stdout).Encode(delResponses)
}

func outputDeleteResponseWide(delResponses []*copperv1.DeleteResponse) error {
	w := tabwriter.NewWriter(os.Stdout, 10, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "%s\t%s\n", "ID", "Deleted")

	for _, delResponse := range delResponses {
		fmt.Fprintf(w, "%d\t%v\n", delResponse.Id, delResponse.IsDeleted)
	}

	return nil
}
