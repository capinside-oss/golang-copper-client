package companies

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	fetchByIDCmd = &cobra.Command{
		Use:   "fetch-by-id",
		Short: "Fetch company by ID",
		RunE:  fetchByIDRunEFunc,
	}
)

func fetchByIDRunEFunc(cmd *cobra.Command, args []string) error {
	ids, err := utils.StringSliceToInt64Slice(args)
	if err != nil {
		return err
	}

	companies, err := fetchByIDs([]int64(ids))
	if err != nil {
		return err
	}

	return output(companies)
}

func fetchByIDs(ids []int64) ([]*copperv1.Company, error) {
	result := make([]*copperv1.Company, len(ids))

	for i, id := range ids {
		company, err := api.FetchCompanyByID(id)
		if err != nil {
			return nil, err
		}
		result[i] = company
	}

	return result, nil
}
