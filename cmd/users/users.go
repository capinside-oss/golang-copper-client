package users

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	usersCmd = &cobra.Command{
		Use:   "users",
		Short: "User commands",
	}
)

func init() {
	usersCmd.AddCommand(searchCmd)
}

// Command returns a pointer to the Cobra Command
func Command() *cobra.Command {
	return usersCmd
}

func output(users []*copperv1.User) error {
	switch viper.GetString("output") {
	case "wide":
		return outputWide(users)
	default:
		return outputJSON(users)
	}
}

func outputJSON(users []*copperv1.User) error {
	return json.NewEncoder(os.Stdout).Encode(users)
}

func outputWide(users []*copperv1.User) error {
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "\n%s\t%s\t%s\t%s", "ID", "Name", "Email", "Groups")

	for _, user := range users {
		groups := make([]string, len(user.Groups))
		for i, group := range user.Groups {
			groups[i] = group.Name
		}
		fmt.Fprintf(w, "\n%d\t%s\t%s\t%s", user.Id, user.Name, user.Email, strings.Join(groups, ","))
	}

	return nil
}
