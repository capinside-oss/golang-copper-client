package customersources

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
)

var (
	listCmd = &cobra.Command{
		Use:   "list",
		Short: "List customer sources",
		RunE:  listRunEFunc,
	}
)

func listRunEFunc(cmd *cobra.Command, args []string) error {
	result, err := api.GetCustomerSources()
	if err != nil {
		return err
	}

	return output(result)
}
