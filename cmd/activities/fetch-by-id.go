package activities

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	fetchByIDCmd = &cobra.Command{
		Use:   "fetch-by-id",
		Short: "Fetch activity by ID",
		RunE:  fetchByIDFunc,
	}
)

func fetchByIDFunc(cmd *cobra.Command, args []string) error {
	ids, err := utils.StringSliceToIntSlice(args)
	if err != nil {
		return err
	}

	activities := []*copperv1.Activity{}

	for _, id := range ids {
		activity, err := api.FetchActivityByID(int64(id))
		if err != nil {
			return err
		}
		activities = append(activities, activity)
	}

	return output(activities, outputWriter)
}
