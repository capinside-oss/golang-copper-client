package pipelines

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	fetchByNameCmd = &cobra.Command{
		Use:   "fetch-by-name",
		Short: "Fectch pipeline(s) by name",
		RunE:  fetchByNameRunEFunc,
	}
)

func fetchByNameRunEFunc(cmd *cobra.Command, args []string) error {
	result := []*copperv1.Pipeline{}

	for _, name := range args {
		pipeline, err := api.FetchPipelineByName(name)
		if err != nil {
			return err
		}
		result = append(result, pipeline)
	}

	return output(result)
}
