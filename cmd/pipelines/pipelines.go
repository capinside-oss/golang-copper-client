package pipelines

import (
	"encoding/json"
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	pipelinesCmd = &cobra.Command{
		Use:   "pipelines",
		Short: "Pipeline commands",
	}
)

func init() {
	pipelinesCmd.AddCommand(
		fetchByNameCmd,
		searchCmd,
	)
}

func Command() *cobra.Command {
	return pipelinesCmd
}

func output(pipelines []*copperv1.Pipeline) error {
	switch viper.GetString("output") {
	case "wide":
		return outputWide(pipelines)
	default:
		return outputJSON(pipelines)
	}
}

func outputWide(pipelines []*copperv1.Pipeline) error {
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "\n%s\t%s", "ID", "Name")

	for _, pipeline := range pipelines {
		fmt.Fprintf(w, "\n%d\t%s", pipeline.Id, pipeline.Name)
	}

	return nil
}

func outputJSON(pipelines []*copperv1.Pipeline) error {
	return json.NewEncoder(os.Stdout).Encode(pipelines)
}
