package pipelines

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	searchCmd = &cobra.Command{
		Use:   "search",
		Short: "Search pipelines",
		RunE:  searchRunE,
	}
)

func searchRunE(cmd *cobra.Command, args []string) error {
	pipelines, err := api.SearchPipelines(copperv1.PipelineSearchParams{})
	if err != nil {
		return err
	}

	return output(pipelines)
}
