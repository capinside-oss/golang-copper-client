package persons

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	activitiesCmd = &cobra.Command{
		Use:   "activities",
		Short: "Fetch persons activities",
		RunE:  activitiesRunEFunc,
	}
)

func activitiesRunEFunc(cmd *cobra.Command, args []string) error {
	if len(args) < 1 {
		return fmt.Errorf("person(id) required")
	}
	ids, err := utils.StringSliceToInt64Slice(args)
	if err != nil {
		return err
	}

	for _, id := range ids {
		params := copperv1.ActivitySearchParams{
			Parent: &copperv1.ActivityParent{
				Id:   id,
				Type: "person",
			},
		}
		activities, err := api.SearchAllActivities(params)
		if err != nil {
			return err
		}
		if err := json.NewEncoder(outputWriter).Encode(activities); err != nil {
			return err
		}
	}

	return nil
}
