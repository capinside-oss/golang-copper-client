package persons

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	fetchByEMailCmd = &cobra.Command{
		Use:   "fetch-by-email",
		Short: "Fetch person(s) by email",
		RunE:  fetchByEmailRunEFunc,
	}
)

func fetchByEmailRunEFunc(cmd *cobra.Command, args []string) error {
	result := []*copperv1.Person{}

	for _, arg := range args {
		person, err := api.FetchPersonByEmail(arg)
		if err != nil && err != api.ErrResourceNotFound {
			return err
		}
		if person != nil {
			result = append(result, person)
		}
	}

	return output(result, outputWriter)
}
