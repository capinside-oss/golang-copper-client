package persons

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	deleteCmd = &cobra.Command{
		Use:   "delete",
		Short: "Delete a person. Argument must be the ID of that person.",
		RunE:  deleteRunEFunc,
	}
)

func deleteRunEFunc(cmd *cobra.Command, args []string) error {
	IDs, err := utils.StringSliceToInt64Slice(args)
	if err != nil {
		return err
	}

	result, err := delete(IDs)
	if err != nil {
		return err
	}

	return outputDeleteResponse(result, outputWriter)
}

func delete(ids []int64) ([]*copperv1.DeleteResponse, error) {
	result := []*copperv1.DeleteResponse{}

	for _, id := range ids {
		resp, err := api.DeletePerson(id)
		if err != nil {
			return nil, err
		}

		result = append(result, resp)
	}

	return result, nil
}
