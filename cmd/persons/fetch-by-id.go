package persons

import (
	"github.com/spf13/cobra"
	api "gitlab.com/capinside/copper-cli/pkg/copper"
	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	fetchByID = &cobra.Command{
		Use:   "fetch-by-id",
		Short: "Fetch person by ID",
		RunE:  fetchByIDRunEFunc,
	}
)

func fetchByIDRunEFunc(cmd *cobra.Command, args []string) error {
	IDs, err := utils.StringSliceToInt64Slice(args)
	if err != nil {
		return err
	}

	result := make([]*copperv1.Person, len(IDs))

	for i, id := range IDs {
		person, err := api.FetchPersonByID(id)
		if err != nil {
			return err
		}
		result[i] = person
	}

	return output(result, outputWriter)
}
