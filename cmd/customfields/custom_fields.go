package customfields

import (
	"encoding/json"
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

var (
	customFieldsCmd = &cobra.Command{
		Use:   "custom-fields",
		Short: "Custom field commands",
	}
)

func init() {
	customFieldsCmd.AddCommand(
		listCmd,
		fetchByIDCmd,
	)
}

// Command returns a pointer to the Command
func Command() *cobra.Command {
	return customFieldsCmd
}

func output(customFieldDefintions []*copperv1.CustomFieldDefinition) error {
	switch viper.GetString("output") {
	case "wide":
		return outputWide(customFieldDefintions)
	default:
		return outputJSON(customFieldDefintions)
	}
}

func outputJSON(customFieldDefinitions []*copperv1.CustomFieldDefinition) error {
	return json.NewEncoder(os.Stdout).Encode(customFieldDefinitions)
}

func outputWide(customFieldDefinitions []*copperv1.CustomFieldDefinition) error {
	w := tabwriter.NewWriter(os.Stdout, 8, 8, 0, '\t', 0)
	defer w.Flush()

	fmt.Fprintf(w, "%s\t%s\t%s", "ID", "Name", "Data type")

	for _, customFieldDef := range customFieldDefinitions {
		fmt.Fprintf(w, "%d\t%s\t%s\n", customFieldDef.Id, customFieldDef.Name, customFieldDef.DataType)
	}

	return nil
}
