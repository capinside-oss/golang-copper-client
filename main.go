package main

import (
	"fmt"
	"os"

	"gitlab.com/capinside/copper-cli/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		fmt.Println(fmt.Errorf("error: %v", err))
		os.Exit(1)
	}
}
