package copper

import (
	"encoding/json"
	"fmt"

	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// ActivitiesPath on API
	ActivitiesPath = "/activities"
	// ActivitiesSearchPath on API
	ActivitiesSearchPath = "/activities/search"
	// ActivityTypesPath on API
	ActivityTypesPath = "/activity_types"

	// ActivityTypeCategorySystem represents a system activity
	ActivityTypeCategorySystem = "system"
	// ActivityTypeCategoryUser represents a user activity
	ActivityTypeCategoryUser = "user"

	// ActivityParentTypeCompany ...
	ActivityParentTypeCompany = "company"
	// ActivityParentTypeLead ...
	ActivityParentTypeLead = "lead"
	// ActivityParentTypeOpportunity ...
	ActivityParentTypeOpportunity = "opportunity"
	// ActivityParentTypePerson ...
	ActivityParentTypePerson = "person"
	// ActivityParentTypeProject ...
	ActivityParentTypeProject = "project"
	// ActivityParentTypeTask ...
	ActivityParentTypeTask = "task"
)

var (
	// Activities is the global activities service
	Activities ActivitiesService
)

// ActivitiesService interface
type ActivitiesService interface {
	Delete(int64) (*copperv1.DeleteResponse, error)
	FetchByID(int64) (*copperv1.Activity, error)
	Search(copperv1.ActivitySearchParams) ([]*copperv1.Activity, error)
	SearchAll(copperv1.ActivitySearchParams) ([]*copperv1.Activity, error)
	Types() (*copperv1.ActivityTypes, error)
}

// ActivityDeleteFunc function header
type ActivityDeleteFunc func(int64) (*copperv1.DeleteResponse, error)

// ActivityFetchByIDFunc function header
type ActivityFetchByIDFunc func(int64) (*copperv1.Activity, error)

// ActivitiesSearchFunc function header
type ActivitiesSearchFunc func(copperv1.ActivitySearchParams) ([]*copperv1.Activity, error)

// ActivityTypesFunc function header
type ActivityTypesFunc func() (*copperv1.ActivityTypes, error)

type activitiesService struct {
	deleteFunc    ActivityDeleteFunc
	fetchByIDFunc ActivityFetchByIDFunc
	searchFunc    ActivitiesSearchFunc
	typesFunc     ActivityTypesFunc
}

// NewActivitiesService returns an instance of activitiesService that
// implements the ActivitiesService interface
func NewActivitiesService(client Client) ActivitiesService {
	return NewActivitiesServiceWithFunc(
		activityDeleteFunc(client),
		activityFetchByIDFunc(client),
		activitiesSearchFunc(client),
		activityTypesFunc(client),
	)
}

// NewActivitiesServiceWithFunc returns an instance of ActivitiesService
// with custom functions
func NewActivitiesServiceWithFunc(deleteFunc ActivityDeleteFunc, fetchByIDFunc ActivityFetchByIDFunc, searchFunc ActivitiesSearchFunc, typesFunc ActivityTypesFunc) ActivitiesService {
	return &activitiesService{
		deleteFunc:    deleteFunc,
		fetchByIDFunc: fetchByIDFunc,
		searchFunc:    searchFunc,
		typesFunc:     typesFunc,
	}
}

func activityTypesFunc(client Client) ActivityTypesFunc {
	return func() (*copperv1.ActivityTypes, error) {
		data, err := client.Get(ActivityTypesPath, nil)
		if err != nil {
			return nil, err
		}

		result := &copperv1.ActivityTypes{}
		return result, json.Unmarshal(data, &result)
	}
}

func activityDeleteFunc(client Client) ActivityDeleteFunc {
	return func(id int64) (*copperv1.DeleteResponse, error) {
		return deleteResourceByIDFunc(client)(ActivitiesPath, id)
	}
}

func activityFetchByIDFunc(client Client) ActivityFetchByIDFunc {
	return func(id int64) (*copperv1.Activity, error) {
		path := fmt.Sprintf("%s/%d", ActivitiesPath, id)
		resp, err := client.Get(path, nil)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Activity{}
		return result, json.Unmarshal(resp, &result)
	}
}

func activitiesSearchFunc(client Client) ActivitiesSearchFunc {
	return func(params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
		data, err := client.Post(ActivitiesSearchPath, &params)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.Activity{}
		return result, json.Unmarshal(data, &result)
	}
}

// Types identify the types of Activities that occur in Copper
func (as *activitiesService) Types() (*copperv1.ActivityTypes, error) {
	return as.typesFunc()
}

// Delete activity matching ID and return an DeleteResponse and nil or nil and an error
func (as *activitiesService) Delete(id int64) (*copperv1.DeleteResponse, error) {
	return as.deleteFunc(id)
}

func (as *activitiesService) FetchByID(id int64) (*copperv1.Activity, error) {
	return as.fetchByIDFunc(id)
}

// Search returns a slice of copperv1.Activity matching params and nil or nil and an error
func (as *activitiesService) Search(params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
	return as.searchFunc(params)
}

// SearchAll returns a slice of all copperv1.Activity matching params and nil or nil and an error
// Walks through all activities by increasing params PageNumber until last page.
func (as *activitiesService) SearchAll(params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
	result := []*copperv1.Activity{}

	params.PageNumber = 1
	params.PageSize = MaxRequestPageSize

	done := false
	for !done {
		activities, err := as.Search(params)
		if err != nil {
			return nil, err
		}

		result = append(result, activities...)
		done = int64(len(activities)) < params.PageSize
		params.PageNumber++
	}

	return result, nil
}

// UnmarshalJSON implements the Unmarshaler interface
// func (a *copperv1.Activity) UnmarshalJSON(data []byte) error {
// 	var result map[string]interface{}
// 	if err := json.Unmarshal(data, &result); err != nil {
// 		return err
// 	}

// 	activity := copperv1.Activity{}
// 	if v, ok := result["activity_date"]; ok {
// 		activity.ActivityDate = utils.Int64Addr(int64(v.(float64)))
// 	}

// 	if v, ok := result["date_created"]; ok {
// 		activity.DateCreated = utils.Int64Addr(int64(v.(float64)))
// 	}
// 	if v, ok := result["date_modified"]; ok {
// 		activity.DateModified = utils.Int64Addr(int64(v.(float64)))
// 	}

// 	if v, ok := result["details"]; ok {
// 		switch v := v.(type) {
// 		case string:
// 			activity.Details = string(v)
// 		default:
// 			if v == nil {
// 				break
// 			}
// 			details := v.(map[string]interface{})
// 			if recipients, ok := details["recipients"].([]interface{}); ok {
// 				activity.Recipients = make([]*copperv1.ActivityDetailsRecipient, 0)
// 				for _, recipient := range recipients {
// 					if email, ok := recipient.(map[string]interface{})["email"]; ok {
// 						activity.Recipients = append(activity.Recipients, &copperv1.ActivityDetailsRecipient{Email: email.(string)})
// 					}
// 				}
// 			}
// 			if sender, ok := details["sender"].(map[string]interface{}); ok {
// 				activity.Sender = &copperv1.ActivityDetailsSender{}
// 				if email, ok := sender["email"]; ok {
// 					activity.Sender.Email = email.(string)
// 				}
// 				if name, ok := sender["name"]; ok {
// 					activity.Sender.Name = name.(string)
// 				}
// 			}
// 			if subject, ok := details["subject"]; ok {
// 				if subject != nil {
// 					activity.Subject = subject.(string)
// 				}
// 			}
// 		}
// 	}

// 	if v, ok := result["id"]; ok {
// 		activity.Id = utils.Int64Addr(v.(int64))
// 	}

// 	if v, ok := result["new_value"]; ok {
// 		activity.NewValue = v.(*anycopperv1.Any)
// 	}

// 	if v, ok := result["old_value"]; ok {
// 		activity.OldValue = v.(*anycopperv1.Any)
// 	}

// 	if v, ok := result["parent"]; ok {

// 		if id, ok := v.(map[string]interface{})["id"]; ok {
// 			activity.Parent.Id = id.(int64)
// 		}
// 		if t, ok := v.(map[string]interface{})["type"]; ok {
// 			activity.Parent.Type = t.(string)
// 		}
// 	}

// 	if v, ok := result["type"]; ok {
// 		if category, ok := v.(map[string]interface{})["category"]; ok {
// 			activity.Type.Category = category.(string)
// 		}
// 		if cai, ok := v.(map[string]interface{})["count_as_interaction"]; ok {
// 			activity.Type.CountAsInteraction = cai.(bool)
// 		}
// 		if id, ok := v.(map[string]interface{})["id"]; ok {
// 			activity.Type.Id = id.(int64)
// 		}
// 		if isDisabled, ok := v.(map[string]interface{})["is_disabled"]; ok {
// 			activity.Type.IsDisabled = isDisabled.(bool)
// 		}
// 		if name, ok := v.(map[string]interface{})["name"]; ok {
// 			activity.Type.Name = name.(string)
// 		}
// 	}

// 	if v, ok := result["user_id"]; ok {
// 		activity.UserId = utils.Int64Addr(v.(int64))
// 	}

// 	*a = activity

// 	return nil
// }

// ActivitiesTypes returns a pointer of copperv1.ActivityTypes and nil
// or nil and an error
func ActivitiesTypes() (*copperv1.ActivityTypes, error) {
	return Activities.Types()
}

// DeleteActivity with id and return a pointer of DeleteResponse and nil
// or nil and an error
func DeleteActivity(id int64) (*copperv1.DeleteResponse, error) {
	return Activities.Delete(id)
}

// FetchActivityByID returns the copperv1.Activity matching id and nil or nil and an error
func FetchActivityByID(id int64) (*copperv1.Activity, error) {
	return Activities.FetchByID(id)
}

// SearchActivities returns a slice of Activities matching params and nil
// or nil and an error. Does not handle pagination.
func SearchActivities(params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
	return Activities.Search(params)
}

// SearchAllActivities returns a slice of Activities matching params and nil
// or nil and an error. Handles pagination.
func SearchAllActivities(params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
	return Activities.SearchAll(params)
}

// IsSystemActivity returns true if type category of a is system else false
func IsSystemActivity(a copperv1.Activity) bool {
	return isActivityType(a, ActivityTypeCategorySystem)
}

// IsUserActivity returns true if type category of a is user else false
func IsUserActivity(a copperv1.Activity) bool {
	return isActivityType(a, ActivityTypeCategoryUser)
}

func isActivityType(a copperv1.Activity, s string) bool {
	return a.Type.Category == s
}
