package copper

import (
	"encoding/json"
	"fmt"

	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// PersonsPath on API
	PersonsPath = "/people"
	// PersonsFetchByEmailPath on API
	PersonsFetchByEmailPath = "/people/fetch_by_email"
	// PersonsSearchPath on API
	PersonsSearchPath = "/people/search"
)

var (
	// Persons is the global persons/peoples service
	Persons PersonsService
)

// PersonsService interface
type PersonsService interface {
	// Activities of person with id matching params returns a slice of copperv1.Activity
	// and nil or nil and an error
	Activities(int64, copperv1.ActivitySearchParams) ([]*copperv1.Activity, error)
	// Create Person and return a pointer of Person and nil or nil and an error
	Create(copperv1.Person) (*copperv1.Person, error)
	// Delete person matching id and return a pointer of DeleteResponse and
	// nil or nil and an error
	Delete(int64) (*copperv1.DeleteResponse, error)
	// FetchByEmail returns a pointer of person matching email and nil
	// or nil and an error
	FetchByEmail(string) (*copperv1.Person, error)
	// FetchByID returns a pointer of person matching id and nil
	// or nil and an error
	FetchByID(int64) (*copperv1.Person, error)
	// Search returns a slice of person matching params and nil
	// or nil and an error
	Search(copperv1.PersonSearchParams) ([]*copperv1.Person, error)
	// SearchAll returns a slice of all person matching params and nil
	// or nil and an error. Handles pagination.
	SearchAll(copperv1.PersonSearchParams) ([]*copperv1.Person, error)
	// Update person matching id with data and return a pointer of person and
	// nil or nil and an error
	Update(int64, copperv1.Person) (*copperv1.Person, error)
}

// PersonActivitiesFunc function header
type PersonActivitiesFunc func(int64, copperv1.ActivitySearchParams) ([]*copperv1.Activity, error)

// PersonCreateFunc function header
type PersonCreateFunc func(copperv1.Person) (*copperv1.Person, error)

// PersonDeleteFunc function header
type PersonDeleteFunc func(int64) (*copperv1.DeleteResponse, error)

// PersonFetchByEmailFunc function header
type PersonFetchByEmailFunc func(string) (*copperv1.Person, error)

// PersonFetchByIDFunc function header
type PersonFetchByIDFunc func(int64) (*copperv1.Person, error)

// PersonsSearchFunc function header
type PersonsSearchFunc func(copperv1.PersonSearchParams) ([]*copperv1.Person, error)

// PersonsSearchAllFunc function header
type PersonsSearchAllFunc func(copperv1.PersonSearchParams) ([]*copperv1.Person, error)

// PersonUpdateFunc function header
type PersonUpdateFunc func(int64, copperv1.Person) (*copperv1.Person, error)

// NewPersonsService returns an instance of personsService that implements
// the PersonsService interface
func NewPersonsService(client Client) PersonsService {
	return NewPersonsServiceWithFunc(
		personActivitiesFunc(client),
		personCreateFunc(client),
		personDeleteFunc(client),
		personFetchByEmailFunc(client),
		personFetchByIDFunc(client),
		personsSearchFunc(client),
		personUpdateFunc(client),
	)
}

// NewPersonsServiceWithFunc returns an instance of personsService that
// implements the PersonsService interface and has custom functions set
func NewPersonsServiceWithFunc(
	activitiesFunc PersonActivitiesFunc,
	createFunc PersonCreateFunc,
	deleteFunc PersonDeleteFunc,
	fetchByEmailFunc PersonFetchByEmailFunc,
	fetchByIDFunc PersonFetchByIDFunc,
	searchFunc PersonsSearchFunc,
	updateFunc PersonUpdateFunc,
) PersonsService {
	return &personsService{
		activitiesFunc:   activitiesFunc,
		createFunc:       createFunc,
		deleteFunc:       deleteFunc,
		fetchByEmailFunc: fetchByEmailFunc,
		fetchByIDFunc:    fetchByIDFunc,
		searchFunc:       searchFunc,
		updateFunc:       updateFunc,
	}
}

type personsService struct {
	activitiesFunc   PersonActivitiesFunc
	createFunc       PersonCreateFunc
	deleteFunc       PersonDeleteFunc
	fetchByEmailFunc PersonFetchByEmailFunc
	fetchByIDFunc    PersonFetchByIDFunc
	searchFunc       PersonsSearchFunc
	updateFunc       PersonUpdateFunc
}

// FetchByEmail fetches the person data and returns nil or an error
func (ps *personsService) FetchByEmail(email string) (*copperv1.Person, error) {
	return ps.fetchByEmailFunc(email)
}

func personFetchByEmailFunc(client Client) PersonFetchByEmailFunc {
	return func(email string) (*copperv1.Person, error) {
		data := copperv1.PersonSearchByEmailParams{
			Email: email,
		}

		resp, err := client.Post(PersonsFetchByEmailPath, &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Person{}
		return result, json.Unmarshal(resp, result)
	}
}

// FetchByID fetches the person matching ID, sets data and returns nil or an error
func (ps *personsService) FetchByID(id int64) (*copperv1.Person, error) {
	return ps.fetchByIDFunc(id)
}

func personFetchByIDFunc(client Client) PersonFetchByIDFunc {
	return func(id int64) (*copperv1.Person, error) {
		resp, err := client.Get(PersonsPathWithID(id), nil)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Person{}
		return result, json.Unmarshal(resp, result)
	}
}

// Search returns a slice of Person matching params and nil or nil and an error
func (ps *personsService) Search(params copperv1.PersonSearchParams) ([]*copperv1.Person, error) {
	return ps.searchFunc(params)
}

func personsSearchFunc(client Client) PersonsSearchFunc {
	return func(params copperv1.PersonSearchParams) ([]*copperv1.Person, error) {
		resp, err := client.Post(PersonsSearchPath, &params)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.Person{}
		return result, json.Unmarshal(resp, &result)
	}
}

// SearchAll returns a slice of Person matching params and nil or nil and an error
// Fetches all persons by increasing params PageNumber until last page.
func (ps *personsService) SearchAll(params copperv1.PersonSearchParams) ([]*copperv1.Person, error) {
	result := []*copperv1.Person{}

	params.PageNumber = 1
	params.PageSize = MaxRequestPageSize

	done := false
	for !done {
		persons, err := ps.Search(params)
		if err != nil {
			return nil, err
		}
		result = append(result, persons...)
		done = int64(len(persons)) < params.PageSize
		params.PageNumber++
	}

	return result, nil
}

// Create creates the person with data and returns the person and nil or nil and an error
func (ps *personsService) Create(data copperv1.Person) (*copperv1.Person, error) {
	return ps.createFunc(data)
}

func personCreateFunc(client Client) PersonCreateFunc {
	return func(data copperv1.Person) (*copperv1.Person, error) {
		resp, err := client.Post(PersonsPath, &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Person{}
		return result, json.Unmarshal(resp, &result)
	}
}

// DeletePerson deletes the person with id and return nil or an error
func (ps *personsService) Delete(id int64) (*copperv1.DeleteResponse, error) {
	return ps.deleteFunc(id)
}

func personDeleteFunc(client Client) PersonDeleteFunc {
	return func(id int64) (*copperv1.DeleteResponse, error) {
		return deleteResourceByIDFunc(client)(PersonsPath, id)
	}
}

// Update updates the data of person with id and returns the updated person and nil or nil and an error
func (ps *personsService) Update(id int64, data copperv1.Person) (*copperv1.Person, error) {
	return ps.updateFunc(id, data)
}

func personUpdateFunc(client Client) PersonUpdateFunc {
	return func(id int64, data copperv1.Person) (*copperv1.Person, error) {
		resp, err := client.Put(PersonsPathWithID(id), &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Person{}
		return result, json.Unmarshal(resp, &result)
	}
}

// Activities returns a slice of activities of people matching id and nil or nil and an error
func (ps *personsService) Activities(id int64, params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
	return ps.activitiesFunc(id, params)
}

func personActivitiesFunc(client Client) PersonActivitiesFunc {
	return func(id int64, params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
		path := fmt.Sprintf("%s/activities", PersonsPathWithID(id))

		resp, err := client.Post(path, &params)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.Activity{}
		return result, json.Unmarshal(resp, &result)
	}
}

// PersonActivities returns a slice of Activities of the Person matching id and params and nil
// or nil and an error
func PersonActivities(id int64, params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
	return Persons.Activities(id, params)
}

// CreatePerson creates a person using data and returns a pointer of Person and nil
// or nil and an error
func CreatePerson(data copperv1.Person) (*copperv1.Person, error) {
	return Persons.Create(data)
}

// DeletePerson deletes the person matching id and returns a pointer of DeleteResponse and nil
// or nil and an error
func DeletePerson(id int64) (*copperv1.DeleteResponse, error) {
	return Persons.Delete(id)
}

// FetchPersonByEmail fetches the person matching email and returns a pointer of Person and nil
// or nil and an error
func FetchPersonByEmail(email string) (*copperv1.Person, error) {
	return Persons.FetchByEmail(email)
}

// FetchPersonByID returns a pointer of the Person matching id and nil
// or nil and an error
func FetchPersonByID(id int64) (*copperv1.Person, error) {
	return Persons.FetchByID(id)
}

// SearchPersons returns a slice of Person matching params and nil
// or nil and an error. Does not handle pagination.
func SearchPersons(params copperv1.PersonSearchParams) ([]*copperv1.Person, error) {
	return Persons.Search(params)
}

// SearchAllPersons returns a slice of Person matching params and nil
// or nil and an error. Handles pagination.
func SearchAllPersons(params copperv1.PersonSearchParams) ([]*copperv1.Person, error) {
	return Persons.SearchAll(params)
}

// UpdatePerson updates the person matching id with data and returns a pointer of Person and nil
// or nil and an error
func UpdatePerson(id int64, data copperv1.Person) (*copperv1.Person, error) {
	return Persons.Update(id, data)
}

// PersonsPathWithID returns a string to the URL path of a person with id
func PersonsPathWithID(id int64) string {
	return fmt.Sprintf("%s/%d", PersonsPath, id)
}
