package copper

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestUnixTimeMarshalJSON(t *testing.T) {
	tt := []struct {
		Time   UnixTime
		Result []byte
		Error  error
	}{
		{
			Time:   UnixTime{},
			Result: []byte{0x2d, 0x36, 0x32, 0x31, 0x33, 0x35, 0x35, 0x39, 0x36, 0x38, 0x30, 0x30},
			Error:  nil,
		},
		{
			Time:   UnixTime(time.Date(1980, 11, 14, 0, 0, 0, 0, time.UTC)),
			Result: []byte{0x33, 0x34, 0x33, 0x30, 0x30, 0x38, 0x30, 0x30, 0x30},
			Error:  nil,
		},
	}

	for _, test := range tt {
		result, err := test.Time.MarshalJSON()
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error, err)
				}
			}
		}
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}
	}
}

func TestUnixTimeUnix(t *testing.T) {
	tt := []struct {
		Time   UnixTime
		Result int64
	}{
		{
			Time:   UnixTime(time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)),
			Result: 0,
		},
		{
			Time:   UnixTime(time.Date(1980, 11, 14, 0, 0, 0, 0, time.UTC)),
			Result: 343008000,
		},
	}

	for _, test := range tt {
		result := test.Time.Unix()
		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}
	}
}
