package copper

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

func TestNewUsersService(t *testing.T) {
	us := NewUsersService(nil)
	assert.NotNil(t, us)
}

func TestSearchUsers(t *testing.T) {
	tt := []struct {
		Params     copperv1.UserSearchParams
		Body       []byte
		StatusCode int
		Result     []*copperv1.User
		Error      error
	}{
		{
			Body: []byte(`[{"id":1,"name":"John Doe","email":"johndoe@example.net"}]`),
			Result: []*copperv1.User{
				{
					Id:    1,
					Name:  "John Doe",
					Email: "johndoe@example.net",
				},
			},
			StatusCode: http.StatusOK,
			Error:      nil,
		},
	}

	for _, test := range tt {
		server := setupTest(UsersSearchPath, test.StatusCode, test.Body)

		result, err := SearchUsers(test.Params)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error.Error(), err.Error())
				}
			}
		}

		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nwant: %v\ngot:  %v", test.Result, result)
		}

		server.Close()
	}
}

func TestFetchUserByID(t *testing.T) {
	tt := []struct {
		ID         int64
		Body       []byte
		StatusCode int
		Result     *copperv1.User
		Error      error
	}{
		{
			ID:         0,
			Body:       []byte(`{"success":false,"status":422,"message":"Invalid input"}`),
			StatusCode: http.StatusUnprocessableEntity,
			Error:      ErrInvalidInput,
			Result:     nil,
		},
		{
			ID:         1,
			Body:       []byte(`{"id":1,"name":"John Doe","email":"johndoe@example.net"}`),
			StatusCode: http.StatusOK,
			Result: &copperv1.User{
				Id:    1,
				Name:  "John Doe",
				Email: "johndoe@example.net",
			},
			Error: nil,
		},
	}

	for _, test := range tt {
		server := setupTest(fmt.Sprintf("%s/%d", UsersPath, test.ID), test.StatusCode, test.Body)

		result, err := FetchUserByID(test.ID)
		if err != nil {
			if test.Error == nil {
				t.Fatalf("unexpected error: %v", err)
			} else {
				if err.Error() != test.Error.Error() {
					t.Fatalf("\nwant: %v\ngot:  %v", test.Error.Error(), err.Error())
				}
			}
		}

		if !assert.Equal(t, test.Result, result) {
			t.Fatalf("\nID: %v,want: %v\ngot:  %v", test.ID, test.Result, result)
		}

		server.Close()
	}
}
