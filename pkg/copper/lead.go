package copper

import (
	"encoding/json"
	"fmt"

	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// LeadsPath on API
	LeadsPath = "/leads"
	// LeadsSearchPath on API
	LeadsSearchPath = "/leads/search"
)

var (
	// Leads is the global leads service
	Leads LeadsService
)

// LeadsService interface
type LeadsService interface {
	Activities(int64, copperv1.ActivitySearchParams) ([]*copperv1.Activity, error)
	Convert(int64, copperv1.LeadConvertData) (*copperv1.LeadConvertResponse, error)
	Create(copperv1.Lead) (*copperv1.Lead, error)
	Delete(int64) (*copperv1.DeleteResponse, error)
	FetchByID(int64) (*copperv1.Lead, error)
	Search(copperv1.LeadSearchParams) ([]*copperv1.Lead, error)
	SearchAll(copperv1.LeadSearchParams) ([]*copperv1.Lead, error)
	Update(int64, copperv1.Lead) (*copperv1.Lead, error)
	Upsert(copperv1.LeadUpsertData) (*copperv1.Lead, error)
}

// LeadActivitiesFunc function header
type LeadActivitiesFunc func(int64, copperv1.ActivitySearchParams) ([]*copperv1.Activity, error)

// LeadConvertFunc function header
type LeadConvertFunc func(int64, copperv1.LeadConvertData) (*copperv1.LeadConvertResponse, error)

// LeadCreateFunc function header
type LeadCreateFunc func(copperv1.Lead) (*copperv1.Lead, error)

// LeadDeleteFunc function header
type LeadDeleteFunc func(int64) (*copperv1.DeleteResponse, error)

// LeadFetchByIDFunc function header
type LeadFetchByIDFunc func(int64) (*copperv1.Lead, error)

// LeadSearchFunc function header
type LeadSearchFunc func(copperv1.LeadSearchParams) ([]*copperv1.Lead, error)

// LeadUpdateFunc function header
type LeadUpdateFunc func(int64, copperv1.Lead) (*copperv1.Lead, error)

// LeadUpsertFunc function header
type LeadUpsertFunc func(copperv1.LeadUpsertData) (*copperv1.Lead, error)

// NewLeadsService returns an instance of leadsService that implements the
// LeadsService interface
func NewLeadsService(client Client) LeadsService {
	return NewLeadsServiceWithFunc(
		leadActivitiesFunc(client),
		leadConvertFunc(client),
		leadCreateFunc(client),
		leadDeleteFunc(client),
		leadFetchByIDFunc(client),
		leadSearchFunc(client),
		leadUpdateFunc(client),
		leadUpsertFunc(client),
	)
}

// NewLeadsServiceWithFunc returns an instance of leadsService that implements
// the LeadsService instance and has custom functions set
func NewLeadsServiceWithFunc(
	activitiesFunc LeadActivitiesFunc,
	convertFunc LeadConvertFunc,
	createFunc LeadCreateFunc,
	deleteFunc LeadDeleteFunc,
	fetchByIDFunc LeadFetchByIDFunc,
	searchFunc LeadSearchFunc,
	updateFunc LeadUpdateFunc,
	upsertFunc LeadUpsertFunc,
) LeadsService {
	return &leadsService{
		activitiesFunc: activitiesFunc,
		convertFunc:    convertFunc,
		createFunc:     createFunc,
		deleteFunc:     deleteFunc,
		fetchByIDFunc:  fetchByIDFunc,
		searchFunc:     searchFunc,
		updateFunc:     updateFunc,
		upsertFunc:     upsertFunc,
	}
}

type leadsService struct {
	activitiesFunc LeadActivitiesFunc
	convertFunc    LeadConvertFunc
	createFunc     LeadCreateFunc
	deleteFunc     LeadDeleteFunc
	fetchByIDFunc  LeadFetchByIDFunc
	searchFunc     LeadSearchFunc
	updateFunc     LeadUpdateFunc
	upsertFunc     LeadUpsertFunc
}

// Create creates a lead and returns the lead and nil or nil and an error
func (ls *leadsService) Create(data copperv1.Lead) (*copperv1.Lead, error) {
	return ls.createFunc(data)
}

func leadCreateFunc(client Client) LeadCreateFunc {
	return func(data copperv1.Lead) (*copperv1.Lead, error) {
		resp, err := client.Post(LeadsPath, &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Lead{}
		return result, json.Unmarshal(resp, &result)
	}
}

// Convert converts a lead to a person and returns the person and nil or nil and an error
// https://developer.copper.com/leads/convert-a-lead.html
func (ls *leadsService) Convert(id int64, convertData copperv1.LeadConvertData) (*copperv1.LeadConvertResponse, error) {
	return ls.convertFunc(id, convertData)
}

func leadConvertFunc(client Client) LeadConvertFunc {
	return func(id int64, data copperv1.LeadConvertData) (*copperv1.LeadConvertResponse, error) {
		path := fmt.Sprintf("%s/%d/convert", LeadsPath, id)

		response, err := client.Post(path, &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.LeadConvertResponse{}
		return result, json.Unmarshal(response, &result)
	}
}

// Update updates the lead with ID with data and returns a pointer
// of lead and nil or nil and an error
func (ls *leadsService) Update(id int64, data copperv1.Lead) (*copperv1.Lead, error) {
	return ls.updateFunc(id, data)
}

func leadUpdateFunc(client Client) LeadUpdateFunc {
	return func(id int64, data copperv1.Lead) (*copperv1.Lead, error) {
		path := fmt.Sprintf("%s/%d", LeadsPath, id)

		resp, err := client.Put(path, &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Lead{}
		return result, json.Unmarshal(resp, result)
	}
}

// FetchByID returns the lead with id nil or nil and an error
func (ls *leadsService) FetchByID(id int64) (*copperv1.Lead, error) {
	return ls.fetchByIDFunc(id)
}

func leadFetchByIDFunc(client Client) LeadFetchByIDFunc {
	return func(id int64) (*copperv1.Lead, error) {
		path := fmt.Sprintf("%s/%d", LeadsPath, id)

		resp, err := client.Get(path, nil)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Lead{}
		return result, json.Unmarshal(resp, result)
	}
}

// Delete the lead with id and return a pointer of DeleteResponse and nil or
// nil and an error
func (ls *leadsService) Delete(id int64) (*copperv1.DeleteResponse, error) {
	return ls.deleteFunc(id)
}

func leadDeleteFunc(client Client) LeadDeleteFunc {
	return func(id int64) (*copperv1.DeleteResponse, error) {
		return deleteResourceByIDFunc(client)(LeadsPath, id)
	}
}

// Search returns a slice of leads matching params and nil or nil and an error
func (ls *leadsService) Search(params copperv1.LeadSearchParams) ([]*copperv1.Lead, error) {
	return ls.searchFunc(params)
}

func leadSearchFunc(client Client) LeadSearchFunc {
	return func(params copperv1.LeadSearchParams) ([]*copperv1.Lead, error) {
		data, err := client.Post(LeadsSearchPath, &params)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.Lead{}
		return result, json.Unmarshal(data, &result)
	}
}

func (ls *leadsService) SearchAll(params copperv1.LeadSearchParams) ([]*copperv1.Lead, error) {
	result := []*copperv1.Lead{}

	params.PageNumber = 1
	params.PageSize = MaxRequestPageSize

	done := false
	for !done {
		leads, err := ls.Search(params)
		if err != nil {
			return nil, err
		}

		result = append(result, leads...)

		done = int64(len(leads)) < params.PageSize
		params.PageNumber++
	}

	return result, nil

}

// Activities returns a slice of activities of lead with id and nil or nil and an error
func (ls *leadsService) Activities(id int64, params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
	return ls.activitiesFunc(id, params)
}

func leadActivitiesFunc(client Client) LeadActivitiesFunc {
	return func(id int64, params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
		path := fmt.Sprintf("%s/%d/activities", LeadsPath, id)

		resp, err := client.Post(path, &params)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.Activity{}
		return result, json.Unmarshal(resp, &result)
	}
}

// Upsert
// "Upsert" (update + insert) will atomically do the following:
// Check for the existence of a Lead matching certain criteria
// If one exists, update it with the supplied parameters.
// If not, create a new Lead with the supplied parameters.
// This is particularly useful to avoid creating duplicate Leads.
func (ls *leadsService) Upsert(data copperv1.LeadUpsertData) (*copperv1.Lead, error) {
	return ls.upsertFunc(data)
}

func leadUpsertFunc(client Client) LeadUpsertFunc {
	return func(data copperv1.LeadUpsertData) (*copperv1.Lead, error) {
		path := fmt.Sprintf("%s/upsert", LeadsPath)

		resp, err := client.Put(path, &data)
		if err != nil {
			return nil, err
		}

		result := &copperv1.Lead{}
		return result, json.Unmarshal(resp, result)
	}
}

// LeadActivities returns a slice of Activities matching params of Lead matching id and nil
// or nil and an error
func LeadActivities(id int64, params copperv1.ActivitySearchParams) ([]*copperv1.Activity, error) {
	return Leads.Activities(id, params)
}

// ConvertLead with id using data and return a pointer of LeadConvertResponse and nil
// or nil and an error
func ConvertLead(id int64, data copperv1.LeadConvertData) (*copperv1.LeadConvertResponse, error) {
	return Leads.Convert(id, data)
}

// CreateLead using data and return a pointer of Lead an nil
// or nil and an error
func CreateLead(data copperv1.Lead) (*copperv1.Lead, error) {
	return Leads.Create(data)
}

// DeleteLead matching id and return a pointer of DeleteResponse and nil
// or nil and an error
func DeleteLead(id int64) (*copperv1.DeleteResponse, error) {
	return Leads.Delete(id)
}

// FetchLeadByID returns a pointer of Lead matching id and nil
// or nil and an error
func FetchLeadByID(id int64) (*copperv1.Lead, error) {
	return Leads.FetchByID(id)
}

// SearchLeads returns a slice of Lead matching params and nil
// or nil and an error
func SearchLeads(params copperv1.LeadSearchParams) ([]*copperv1.Lead, error) {
	return Leads.Search(params)
}

// SearchAllLeads returns a slice of Lead matching params and nil
// or nil and an error. Handles pagination.
func SearchAllLeads(params copperv1.LeadSearchParams) ([]*copperv1.Lead, error) {
	return Leads.SearchAll(params)
}

// UpdateLead matching id  using data and return a pionter of Lead and nil
// or nil and an error.
func UpdateLead(id int64, data copperv1.Lead) (*copperv1.Lead, error) {
	return Leads.Update(id, data)
}

// UpsertLead creates the Lead of none exists or updates the existing one and returns
// a pointer of Lead and nil or nil and an error.
func UpsertLead(data copperv1.LeadUpsertData) (*copperv1.Lead, error) {
	return Leads.Upsert(data)
}
