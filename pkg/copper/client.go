package copper

// https://developer.copper.com/index.html

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"

	"github.com/sirupsen/logrus"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
)

const (
	// EnvironmentVariableAPIAccount name
	EnvironmentVariableAPIAccount = "COPPER_API_ACCOUNT"
	// EnvironmentVariableAPIToken name
	EnvironmentVariableAPIToken = "COPPER_API_TOKEN"
	// EnvironmentVariableAPIURL name
	EnvironmentVariableAPIURL = "COPPER_API_URL"
)

var (
	c Client

	ErrAuthentication   = fmt.Errorf("authentication error")
	ErrInvalidInput     = fmt.Errorf("Invalid input")
	ErrResourceNotFound = fmt.Errorf("Resource not found")
)

// Client interface
type Client interface {
	Delete(string, proto.Message) ([]byte, error)
	Get(string, proto.Message) ([]byte, error)
	Post(string, proto.Message) ([]byte, error)
	Put(string, proto.Message) ([]byte, error)
}

// ClientDeleteFunc function header
type ClientDeleteFunc func(string, proto.Message) ([]byte, error)

// ClientGetFunc function header
type ClientGetFunc func(string, proto.Message) ([]byte, error)

// ClientHeaderFunc function header
type ClientHeaderFunc func() http.Header

// ClientPostFunc function header
type ClientPostFunc func(string, proto.Message) ([]byte, error)

// ClientPutFunc function header
type ClientPutFunc func(string, proto.Message) ([]byte, error)

type client struct {
	deleteFunc ClientDeleteFunc
	getFunc    ClientGetFunc
	postFunc   ClientPostFunc
	putFunc    ClientPutFunc
}

// Config data
type Config struct {
	Account string
	BaseURL *url.URL
	Token   string
}

func init() {
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetOutput(os.Stdout)

	baseURL := lookupEnv(EnvironmentVariableAPIURL, BaseURLV1)
	account := lookupEnv(EnvironmentVariableAPIAccount, "")
	token := lookupEnv(EnvironmentVariableAPIToken, "")

	if err := Init(baseURL, account, token, "info"); err != nil {
		panic(err)
	}
}

// Init initializes the global client
func Init(baseURL string, account string, token string, logLevel string) error {
	cfg, err := NewConfig(baseURL, account, token)
	if err != nil {
		return fmt.Errorf("new config: %v", err)
	}

	logLevelParsed, err := logrus.ParseLevel(logLevel)
	if err != nil {
		return fmt.Errorf("parsing log level '%s': %v", logLevel, err)
	}
	logrus.SetLevel(logLevelParsed)

	logrus.WithFields(logrus.Fields{"url": baseURL, "account": account, "log-level": logLevel}).Debug("init")

	c = NewClient(*cfg)

	Activities = NewActivitiesService(c)
	Companies = NewCompaniesService(c)
	CustomFields = NewCustomFieldDefinitionsService(c)
	CustomerSources = NewCustomerSourcesService(c)
	Leads = NewLeadsService(c)
	Opportunities = NewOpportunitiesService(c)
	Persons = NewPersonsService(c)
	Pipelines = NewPipelinesService(c)
	Users = NewUsersService(c)

	return nil
}

func lookupEnv(key string, defaultValue string) string {
	value, found := os.LookupEnv(key)
	if found {
		return value
	}
	return defaultValue
}

// NewConfig returns an instance of Config and nil or nil and an error if
// baseURL can't be parsed
func NewConfig(baseURL string, account string, token string) (*Config, error) {
	configBaseURL, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}

	return &Config{
		BaseURL: configBaseURL,
		Account: account,
		Token:   token,
	}, nil
}

// NewClient returns an instance of client that implements the Client interface
// and has default functions set
func NewClient(config Config) Client {
	headerFunc := getHeaderFunc(config.Account, config.Token)
	httpClient := http.DefaultClient
	return NewClientWithFunc(
		clientDeleteFunc(httpClient, config.BaseURL, headerFunc),
		clientGetFunc(httpClient, config.BaseURL, headerFunc),
		clientPostFunc(httpClient, config.BaseURL, headerFunc),
		clientPutFunc(httpClient, config.BaseURL, headerFunc),
	)
}

// NewClientWithFunc returns an instance of client that implements the Client
// interface and has custom functions set
func NewClientWithFunc(deleteFunc ClientDeleteFunc, getFunc ClientGetFunc, postFunc ClientPostFunc, putFunc ClientPutFunc) Client {
	return &client{
		deleteFunc: deleteFunc,
		getFunc:    getFunc,
		postFunc:   postFunc,
		putFunc:    putFunc,
	}
}

func (c *client) Delete(path string, v proto.Message) ([]byte, error) {
	return c.deleteFunc(path, v)
}

func clientDeleteFunc(httpClient *http.Client, baseURL *url.URL, headerFunc ClientHeaderFunc) ClientDeleteFunc {
	return func(path string, payload proto.Message) ([]byte, error) {
		return request(httpClient, baseURL, headerFunc, http.MethodDelete, path, nil)
	}
}

func (c *client) Get(path string, payload proto.Message) ([]byte, error) {
	return c.getFunc(path, payload)
}

func clientGetFunc(httpClient *http.Client, baseURL *url.URL, headerFunc ClientHeaderFunc) ClientGetFunc {
	return func(path string, payload proto.Message) ([]byte, error) {
		return request(httpClient, baseURL, headerFunc, http.MethodGet, path, payload)
	}
}

func (c *client) Put(path string, payload proto.Message) ([]byte, error) {
	return c.putFunc(path, payload)
}

func clientPutFunc(httpClient *http.Client, baseURL *url.URL, headerFunc ClientHeaderFunc) ClientPutFunc {
	return func(path string, payload proto.Message) ([]byte, error) {
		return request(httpClient, baseURL, headerFunc, http.MethodPut, path, payload)
	}
}

func (c *client) Post(path string, payload proto.Message) ([]byte, error) {
	return c.postFunc(path, payload)
}

func clientPostFunc(httpClient *http.Client, baseURL *url.URL, headerFunc ClientHeaderFunc) ClientPostFunc {
	return func(path string, payload proto.Message) ([]byte, error) {
		return request(httpClient, baseURL, headerFunc, http.MethodPost, path, payload)
	}
}

func request(httpClient *http.Client, baseURL *url.URL, headerFunc ClientHeaderFunc, method string, urlPath string, payload proto.Message) ([]byte, error) {
	m := protojson.MarshalOptions{
		UseProtoNames: true,
	}
	body, err := m.Marshal(payload)
	if err != nil {
		return nil, err
	}

	uri, err := baseURL.Parse(path.Join(baseURL.Path, urlPath))
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(method, uri.String(), bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	logrus.WithFields(logrus.Fields{"uri": uri.Path, "values": req.URL.RawQuery}).Debug("executing request")

	req.Header = headerFunc()

	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	logrus.WithFields(logrus.Fields{
		"status": resp.StatusCode,
		"body":   string(respBody),
	}).Debug("response")

	return handleAPIResponse(resp, respBody)

}

func getHeaderFunc(account string, token string) func() http.Header {
	return func() http.Header {
		header := http.Header{}
		header.Set("Content-Type", "application/json; charset=UTF-8")
		header.Set("X-PW-AccessToken", token)
		header.Set("X-PW-Application", "developer_api")
		header.Set("X-PW-UserEmail", account)

		return header
	}
}

func handleAPIResponse(resp *http.Response, body []byte) ([]byte, error) {
	if resp.StatusCode == http.StatusOK {
		return body, nil
	}

	if resp.StatusCode >= 500 {
		return nil, fmt.Errorf(http.StatusText(resp.StatusCode))
	}

	if resp.StatusCode == http.StatusUnauthorized {
		return nil, ErrAuthentication
	}

	apiErr := copperv1.Error{}
	if err := protojson.Unmarshal(body, &apiErr); err != nil {
		return nil, fmt.Errorf("unmarshal proto error: %v", err)
	}

	return nil, fmt.Errorf(apiErr.Message)
}
