package copper

import (
	"encoding/json"
	"fmt"

	utils "gitlab.com/capinside/copper-cli/pkg/utils"
	copperv1 "gitlab.com/capinside/protobuf-api/languages/golang/copper/v1"
)

const (
	// CustomFieldDefinitionsPath on API
	CustomFieldDefinitionsPath = "/custom_field_definitions"

	CustomFieldTypeCheckbox    = "Checkbox"
	CustomFieldTypeConnect     = "Connect"
	CustomFieldTypeCurrency    = "Currency"
	CustomFieldTypeDate        = "Date"
	CustomFieldTypeDropdown    = "Dropdown"
	CustomFieldTypeFloat       = "Float"
	CustomFieldTypeMultiSelect = "MultiSelect"
	CustomFieldTypePercentage  = "Percentage"
	CustomFieldTypeString      = "String"
	CustomFieldTypeText        = "Text"
	CustomFieldTypeURL         = "URL"
)

var (
	// CustomFields is the global custom fields service
	CustomFields CustomFieldDefinitionsService
)

// CustomFieldDefinitionsService interface
type CustomFieldDefinitionsService interface {
	// CustomFieldDefinitions returns a slice of CustomFieldDefinition and nil
	// or nil and an error
	CustomFieldDefinitions() ([]*copperv1.CustomFieldDefinition, error)
	// Fetch returns a pointer of CustomFieldDefition matching id and nil
	// or nil and an error
	FetchByID(id int64) (*copperv1.CustomFieldDefinition, error)
}

// CustomFieldDefinitionsFunc function header
type CustomFieldDefinitionsFunc func() ([]*copperv1.CustomFieldDefinition, error)

// CustomFieldDefinitionsFetchByIDFunc function header
type CustomFieldDefinitionsFetchByIDFunc func(int64) (*copperv1.CustomFieldDefinition, error)

type customFieldDefinitionsService struct {
	customFieldDefinitionsFunc CustomFieldDefinitionsFunc
	fetchByIDFunc              CustomFieldDefinitionsFetchByIDFunc
}

// NewCustomFieldDefinitionsService returns an instance of customFieldDefinitionsService
// that implements the CustomFieldDefinitionsService with default functions set.
func NewCustomFieldDefinitionsService(client Client) CustomFieldDefinitionsService {
	return NewCustomFieldDefinitionsServiceWithFunc(
		customFieldDefinitionsFunc(client),
		customFieldDefinitionsFetchByIDFunc(client),
	)
}

// NewCustomFieldDefinitionsServiceWithFunc returns an instance of customFieldDefinitionsService
// that implements the CustomFieldDefinitionsService interface and has custom functions set.
func NewCustomFieldDefinitionsServiceWithFunc(customFieldDefinitionsFunc CustomFieldDefinitionsFunc, fetchByIDFunc CustomFieldDefinitionsFetchByIDFunc) CustomFieldDefinitionsService {
	return &customFieldDefinitionsService{
		customFieldDefinitionsFunc: customFieldDefinitionsFunc,
		fetchByIDFunc:              fetchByIDFunc,
	}
}

// CustomField data
type CustomField struct {
	// The id of the Custom Field Definition for which this Custom Field stores a value.
	CustomFieldDefinitionID int `json:"custom_field_definition_id,omitempty"`
	// The value (number, string, option id, or timestamp) of this Custom Field.
	Value interface{} `json:"value,omitempty"`
}

// CustomFieldDefinitions returns a slice of CustomFieldDefintion and nil or nil and an error
func (cfds *customFieldDefinitionsService) CustomFieldDefinitions() ([]*copperv1.CustomFieldDefinition, error) {
	return cfds.customFieldDefinitionsFunc()
}

func customFieldDefinitionsFunc(client Client) CustomFieldDefinitionsFunc {
	return func() ([]*copperv1.CustomFieldDefinition, error) {
		data, err := client.Get(CustomFieldDefinitionsPath, nil)
		if err != nil {
			return nil, err
		}

		result := []*copperv1.CustomFieldDefinition{}
		return result, json.Unmarshal(data, &result)
	}
}

// FetchByID returns the custom field defintion matching id and nil or nil and an error
func (cfds *customFieldDefinitionsService) FetchByID(id int64) (*copperv1.CustomFieldDefinition, error) {
	return cfds.fetchByIDFunc(id)
}

func customFieldDefinitionsFetchByIDFunc(client Client) CustomFieldDefinitionsFetchByIDFunc {
	return func(id int64) (*copperv1.CustomFieldDefinition, error) {
		path := fmt.Sprintf("%s/%d", CustomFieldDefinitionsPath, id)

		data, err := client.Get(path, nil)
		if err != nil {
			return nil, err
		}

		result := &copperv1.CustomFieldDefinition{}
		return result, json.Unmarshal(data, &result)
	}
}

type CustomFieldDefinitions []*copperv1.CustomFieldDefinition

// FetchByID returns a pointer to the custom field with id or nil
func (cfds *CustomFieldDefinitions) FetchByID(id int64) *copperv1.CustomFieldDefinition {
	var result *copperv1.CustomFieldDefinition

	for _, cfd := range *cfds {
		if cfd.Id != nil && *cfd.Id == id {
			result = cfd
			break
		}
	}

	return result
}

// GetCustomFieldDefinitions returns a slice of CustomFieldDefintion and nil
// or nil and an error
func GetCustomFieldDefinitions() ([]*copperv1.CustomFieldDefinition, error) {
	return CustomFields.CustomFieldDefinitions()
}

// FetchCustomFieldDefinitionByID returns a pointer of CustomFieldDefintion matching id and nil
// or nil and an error
func FetchCustomFieldDefinitionByID(id int64) (*copperv1.CustomFieldDefinition, error) {
	return CustomFields.FetchByID(id)
}

// IsCustomFieldDefinitionAvailableOn returns true if custom field definition is available for name
func IsCustomFieldDefinitionAvailableOn(cfd copperv1.CustomFieldDefinition, name string) bool {
	return utils.StringInStringSlice(name, cfd.AvailableOn)
}
