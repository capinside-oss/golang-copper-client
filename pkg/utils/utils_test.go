package copper

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringSliceToIntSlice(t *testing.T) {}

func TestStringInStringSlice(t *testing.T) {}

func TestIntAddr(t *testing.T) {
	i := 0
	result := IntAddr(i)
	assert.Equal(t, &i, result)
}

func TestInt64Addr(t *testing.T) {
	i := int64(0)
	result := Int64Addr(i)
	assert.Equal(t, &i, result)
}

func TestFloat64Addr(t *testing.T) {
	f := float64(0)
	result := Float64Addr(f)
	assert.Equal(t, &f, result)
}
